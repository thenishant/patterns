package withPOM;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class SearchTest extends Driver {

    @Test
    public void searchTicketTest() {

        WebDriver driver = getDriver();
        SearchPage searchPage = new SearchPage(driver);
        driver.findElement(searchPage.acceptCookies).click();

        driver.findElement(searchPage.searchFrom).sendKeys("London");
        driver.findElement(searchPage.searchTo).sendKeys("Leeds");
        driver.findElement(searchPage.oneWay).click();
        driver.findElement(searchPage.nextDay).click();
        driver.findElement(searchPage.searchTickets).click();
    }

}
