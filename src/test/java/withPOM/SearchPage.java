package withPOM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertEquals;

public class SearchPage {
    WebDriver driver;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }

    By acceptCookies = By.cssSelector("button[id=onetrust-accept-btn-handler]");
    By searchFrom = By.id("from.search");
    By searchTo = By.id("to.search");
    By oneWay = By.id("single");
    By nextDay = By.cssSelector("button[data-test='datepicker-next-day-button']");
    By searchTickets = By.cssSelector("button[data-test='submit-journey-search-button']");

    public void searchForm(String fromStation) {
        driver.findElement(searchFrom).sendKeys(fromStation);
    }

    public void toStation(String toStation) {
        driver.findElement(searchTo).sendKeys(toStation);
    }

    public void oneWayJourney(){
        driver.findElement(oneWay).click();
    }

    public void nextDayJourney(){
        driver.findElement(nextDay).click();
    }

    public void searchTicket(){
        driver.findElement(searchTickets).click();
    }
}
