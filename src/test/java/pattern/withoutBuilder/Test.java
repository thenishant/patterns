package pattern.withoutBuilder;

public class Test {
    public static void main(String[] args) {


        String firstName = "Nishant";
        String lastName = "Sharma";
        String city = "Bangalore";
        Employee employee = new Employee(firstName);
        Employee employee1 = new Employee(firstName, lastName);
        Employee employee2 = new Employee(firstName, lastName, city);

        System.out.println(employee);
        System.out.println(employee1);
    }
}
