package pattern.withBuilder;

public class Test {
    public static void main(String[] args) {
        String firstName = "Nishant";
        String lastName = "Sharma";
        String city = "Bangalore";

        Employee employee = new Employee.EmployeeBuilder()
                .firstName(firstName)
                .lastName(lastName)
                .age(20)
                .build();

        System.out.println(employee);
    }
}
