package withoutPOM;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.time.Duration;

import static org.testng.Assert.assertEquals;

public class SearchTrainTest {
    private WebDriver driver;

    @BeforeSuite
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeClass
    public void setupTest() {
        driver = new ChromeDriver();
    }

    @AfterClass
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void test() {
        driver.get("https://thetrainline.com");

        By acceptCookies = By.cssSelector("button[id=onetrust-accept-btn-handler]");
        By searchFrom = By.id("from.search");
        By searchTo = By.id("to.search");
        By oneWay = By.id("single");
        By nextDay = By.cssSelector("button[data-test='datepicker-next-day-button']");
        By searchTickets = By.cssSelector("button[data-test='submit-journey-search-button']");

        By byTicketPrice = By.xpath("//*[@data-test='tab-train-price']");
        By byTotalPrice = By.xpath("//*[@data-test='cjs-price']");

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.findElement(acceptCookies).click();

        driver.findElement(searchFrom).sendKeys("London");
        driver.findElement(searchTo).sendKeys("Leeds");
        driver.findElement(oneWay).click();

        driver.findElement(nextDay).click();
        driver.findElement(searchTickets).click();

        String price = driver.findElement(byTicketPrice).getText();
        String totalPrice = driver.findElement(byTicketPrice).getText();

        assertEquals(price,totalPrice);
    }
}
